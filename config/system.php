<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * IDEAWEB
 */

/**
 * 系统配置文件
 */

return array (
    'SYS_LOG' => 1,
    'SYS_KEY' => 'CI351E5C6BCC91D4',
    'SYS_DEBUG' => '',
    'SYS_EMAIL' => '308058744@qq.com',
    'SYS_AUTO_CACHE' => 1,
    'SITE_ADMIN_CODE' => '',
    'SITE_ADMIN_PAGESIZE' => '100',
    'SYS_CACHE_INDEX' => 3600,
    'SYS_CACHE_MSHOW' => 3600,
    'SYS_CACHE_MSEARCH' => 3600,
    'SYS_CACHE_LIST' => 3600,
    'SYS_CACHE_USER' => 3600,
    'SYS_CACHE_ATTACH' => 3600,
    'SYS_CACHE_FORM' => 3600,
    'SYS_CACHE_TAG' => 3600,
    'SYS_CAT_MODULE' => '',
);