function dd_alert(msg) {
	dd_tips(msg, 9);
}

function dd_diy_type(t) {
	$("#dd_diy_type_0").hide();
	$("#dd_diy_type_1").hide();
	$("#dd_diy_type_" + t).show()
}

function dd_member_rule(id, url, title) {
	$.ajax({
		type: "GET",
		url: url,
		dataType: "text",
		success: function(text) {
			var d = top.dialog({
				title: title,
				content: text,
				okValue: lang['ok'],
				ok: function () {
					var that = this;
					that.title(''+lang['ing']);
					top.$("#mark").val("0");
					if (top.dd_form_check()) {
						var _data = top.$("#myform").serialize();
						$.ajax({
							type: "POST",
							dataType: "text",
							url: url,
							data: _data,
							success: function(data) {
								d.close().remove();
								$("#dd_status_" + id).html(" <i class='fa fa-check-square'></i>");
								dd_tips(fc_lang[37], 2, 1);
							},
							error: function(HttpRequest, ajaxOptions, thrownError) {

							}
						})
					}

					return false;
				},
				cancelValue: lang['cancel'],
				cancel: function () {
					return true;
				}
			});
			d.show();
		},
		error: function(HttpRequest, ajaxOptions, thrownError) {
			dd_alert(HttpRequest.responseText)
		}
	});
}

function dd_remove_file(name, id) {

    layer.alert(lang["confirm"],{
        title: lang["tips"],
        time: 0,
        shadeClose: true,
        btn:['确定','取消'],
        yes: function(index){
            var fileid = $("#fileid_" + name + "_" + id).val();
            var value = $("#dd_" + name + "_del").val();
            $("#files_" + name + "_" + id).remove();
            $("#dd_" + name + "_del").val(value + "|" + fileid);
            layer.close(index);
        }
    });
}




function dd_dialog_member(id) {
	if (id == "author") {
		id = $("#dd_author").val()
	}
	layer.open({
	  type: 2,
	  title: '用户信息',
	  shadeClose: true,
	  shade: 0.8,
	  area: ['380px', '60%'],
	  content: siteurl + "index.php?s=admin&c=api&m=member&uid=" + id
	});
}


function dd_dialog_ip(id) {
	var name = $("#dd_" + id).val();
	if (name) {
		layer.open({
		  type: 2,
		  title: 'IP',
		  shadeClose: true,
		  shade: 0.8,
		  area: ['380px', '60%'],
		  content: "http://www.baidu.com/baidu?wd=" + name
		});
	}else{

		layer.alert('见到你真的很高兴', {icon: 6});
		dd_tips("[" + id + "] " + lang["iperror"], 3)
	}
	
}


function dd_page_rule() {
	var body = '<table border="0" cellpadding="1" cellspacing="0" class="layui-table" lay-size="sm">';
	body += "<tr><td>{id}</td><td>Id</td></tr><tr>";
	body += "<tr><td>{page}</td><td>" + lang["page"] + "</td></tr>";
	body += "<tr><td>{dirname}</td><td>" + lang["dirname"] + "</td></tr>";
	body += "<tr><td>{pdirname}</td><td>" + lang["pdirname"] + "</td></tr>";
	body += "<tr><td>{fid}</td><td>" + lang["fid"] + "</td></tr>";
	body += "</table>&nbsp;";
	layer.open({
	  type: 1,
	  title: lang["tagseo"],
	  area:['80%','80%'],
	  shadeClose: true,
	  content: body
	});
}

function dd_url_rule() {
	var body = '<table border="0" cellpadding="1" cellspacing="0" class="layui-table" lay-size="sm">';
	body += '<tr><td width="15%">' + lang["tag"] + '</td><td width="85%">&nbsp;</td></tr>';
	body += "<tr><td>{id}</td><td>Id</td></tr><tr>";
	body += "<tr><td>{page}</td><td>" + lang["page"] + "</td></tr>";
	body += "<tr><td>{modname}</td><td>" + lang["modname"] + "</td></tr>";
	body += "<tr><td>{dirname}</td><td>" + lang["dirname"] + "</td></tr>";
	body += "<tr><td>{pdirname}</td><td>" + lang["pdirname"] + "</td></tr>";
	body += "<tr><td>{fid}</td><td>" + lang["fid"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["tagvalue"] + "</td></tr>";
	body += "<tr><td>" + lang["function"] + "</td><td>&nbsp;</td></tr>";
	body += "<tr><td>{md5({id})}</td><td>" + lang["funcvalue1"] + "</td></tr>";
	body += "<tr><td>{test($data)}</td><td>" + lang["funcvalue2"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["tagmore"] + "</td></tr>";
	body += "</table>";
	layer.open({
	  type: 1,
	  title: lang["tagseo"],
	  area:['80%','80%'],
	  shadeClose: true,
	  content: body
	});
}


function dd_seo_rule() {
	var body = '<table border="0" cellpadding="1" cellspacing="0" class="layui-table" lay-size="sm">';
	body += '<tr><td width="15%">' + lang["tag"] + '</td><td width="85%">&nbsp;</td></tr>';
	body += "<tr><td>{join}</td><td>" + lang["seojoin"] + "</td></tr><tr>";
	body += "<tr><td>{modulename}</td><td>" + lang["seoname"] + "</td></tr><tr>";
	body += "<tr><td>[{page}]</td><td>" + lang["seopage"] + "</td></tr>";
	body += "<tr><td>{SITE_NAME}</td><td>" + lang["seositename"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["seovalue1"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["seovalue2"] + "</td></tr>";
	body += "<tr><td>" + lang["function"] + "</td><td>&nbsp;</td></tr>";
	body += "<tr><td>{test($data)}</td><td>" + lang["seofuncvalue"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["seodiy"] + "</td></tr>";
	body += "<tr><td>&nbsp;</td><td>" + lang["tagmore"] + "</td></tr>";
	body += "</table>";

	layer.open({
	  type: 1,
	  title: lang["tagseo"],
	  area:['80%','80%'],
	  shadeClose: true,
	  content: body
	});
}


function set_frontop(v) {
	if (v == 1) {
		$(".tabBut li:gt(1)").show()
	} else {
		$(".tabBut li:gt(1)").hide()
	}
}
function set_urlmode(v) {
	if (v == 1) {
		$("#urlmode").show()
	} else {
		$("#urlmode").hide()
	}
}
function set_sitemode(v) {
	if (v == 1) {
		$("#sitemode").show()
	} else {
		$("#sitemode").hide()
	}
}
function set_urltohtml(v) {
	if (v == 1) {
		$("#html").show()
	} else {
		$("#html").hide()
	}
}

function dd_form_tips(name, status, code) {
	var obj = $("#dd_" + name + "_tips");
	obj.html("");
	if (status) {
		obj.attr("class", "");
		dd_tips(code, 3, 1)
	} else {
		obj.attr("class", "");
		dd_tips(code)
	}
}


function dd_selected_by(id) {
	if ($("#" + id).prop("checked")) {
		$("." + id).prop("checked", true);
	} else {
		$("." + id).prop("checked", false);
	}
}
function dd_goto_url(url) {
	window.location.href = url;
}
function dd_waiting() {
	dd_tips(lang["waiting"], 3, 1);
}
function dd_dialog_show(title, url) {
	$.ajax({
		type: "POST",
		dataType: "text",
		url: url,
		data: {},
		success: function(data) {
			top.dialog({
				quickClose: true,
				content: data,
				title: title
			}).show();
		},
		error: function(HttpRequest, ajaxOptions, thrownError) {}
	})
}


function dd_dialog_set(text, url) {

	layer.confirm(text, {

      btn: ['确定','取消'] //按钮

    }, function(){

		$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			data: {},
			success: function(data) {
				if (data.code == 1) {
					dd_tips(data.status, 1, 1);
					setTimeout("window.location.reload(true)", 2000)
				} else {
					dd_tips(data.status, 2, 1);
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {
			}
		});

    }, function(){
    	//取消回调

    });

}


function dd_confirm_set_all(title, del) {

	layer.confirm(title, {
	  title:lang['tips'],
	  btn: ['确定','取消'] //按钮
	}, function(index,layero){

	  	var _data = $("#myform").serialize();
		var _url = window.location.href;
		if ((_data.split("ids")).length - 1 <= 0) {
			dd_tips(lang["select_null"]);
			return false;
		}
		$.ajax({
			type: "POST",
			dataType: "json",
			url: _url,
			data: _data,
			success: function(data) {
				if (data.status == 1) {
					dd_tips(data.code, 3, 1);
					if (del == 1) {
						$(".dd_select").each(function() {
							if ($(this).attr("checked")) {
								$("#dd_row_" + $(this).val()).remove()
							}
						})
					} else {
						setTimeout("window.location.reload(true)", 1000)
					}
				} else {
					dd_tips(data.code, 3, 2);
					return true
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {}
		});

	}, function(){
	});
}

function dd_dialog_del(text, url) {

	layer.alert(text,{
	  type: 1,
	  title:lang["del"],
	  area: '300px;',
	  btn: ['确定', '取消'],
	  yes: function(index){
	  	$.ajax({
			type: "POST",
			dataType: "json",
			url: url,
			data: {},
			success: function(data) {
				if (data.status == 1) {
					dd_tips(data.code, 3, 1);
					setTimeout("window.location.reload(true)", 2000)
				} else {
					dd_tips(data.code, 2, 2);
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {}
		});

	    layer.close(index);
	  }
	});
}

function dd_confirm_del(){
	$('#action').val('del');
	var _data = $("#myform").serialize();
	var _url = window.location.href;
	if ((_data.split("ids")).length - 1 <= 0) {
		dd_tips(lang["select_null"], 2);
		return true
	}
	$.ajax({
		type: "POST",
		dataType: "json",
		url: _url,
		data: _data,
		success: function(data) {
			if (data.status == 1) {
				dd_tips(data.code, 2, 1);
				setTimeout("window.location.reload(true)", 2000)
			} else {
				dd_tips(data.code, 3, 2);
				return true
			}
		},
		error: function(HttpRequest, ajaxOptions, thrownError) {}
	});
	return true
}

function dd_confirm_verify(){
	$('#action').val('verify');
	var _data = $("#myform").serialize();
	var _url = window.location.href;
	if ((_data.split("ids")).length - 1 <= 0) {
		dd_tips(lang["select_null"], 2);
		return true
	}
	$.ajax({
		type: "POST",
		dataType: "json",
		url: _url,
		data: _data,
		success: function(data) {
			if (data.status == 1) {
				dd_tips(data.code, 3, 1);
				setTimeout("window.location.reload(true)", 1000)
			} else {
				dd_tips(data.code, 3, 2);
				return true
			}
		},
		error: function(HttpRequest, ajaxOptions, thrownError) {}
	});
	return true
}

function dd_confirm_order() {
	layer.confirm(lang["confirm"], {
		title:"提示",
	  	btn: ['确定','取消'] 
	}, function(){

		$('#action').val('order');
		var _data = $("#myform").serialize();
		var _url = window.location.href;
		if ((_data.split("ids")).length - 1 <= 0) {
			dd_tips(lang["select_null"], 2);
			return true
		}
		$.ajax({
			type: "POST",
			dataType: "json",
			url: _url,
			data: _data,
			success: function(data) {
				if (data.status == 1) {
					dd_tips(data.code, 3, 1);
					setTimeout("window.location.reload(true)", 1000)
				} else {
					dd_tips(data.code, 3, 2);
					return true
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {}
		});
		return true

	},function(){

	});

}

function dd_tips(msg, time, mark) {
    if (!msg || msg == '' || msg == '&nbsp;') {
        return
    }
    var mymsg;
    if (mark == 1) {
        icon = 6
    } else if (mark == 2) {
        icon = 1
    } else {
        icon = 5
    }
    if (!time) {
        time = 1
    }

    layer.msg(msg, {
        time: time * 1000, //20s后自动关闭
        icon: icon,
    });
}

function dd_confirm_del_all() {

	layer.confirm(lang["confirm"], {
		title:"提示",
	  	btn: ['确定','取消'] 
	}, function(){

		var _data = $("#myform").serialize();
		var _url = window.location.href;
		if ((_data.split("ids")).length - 1 <= 0) {
			dd_tips(lang["select_null"], 2);
			return true;
		}
		$.ajax({
			type: "POST",
			dataType: "json",
			url: _url,
			data: _data,
			success: function(data) {
				if (data.status == 1) {
					dd_tips(data.code, 2, 1);
					setTimeout("window.location.reload(true)", 2000)
				} else {
					dd_tips(data.code, 2, 2);
					return true
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {}
		});
		return true
	}, function(){
	});
}

function dd_dialog(url, func) {
	switch (func) {
		case "add":
			var _title = lang["add"];
			break;
		case "edit":
			var _title = lang["edit"];
			break;
		default:
			return false;
			break
	}

	layer.open({
	 	type: 2,
	 	title: _title,
	 	area: ['60%', '70%'],
	 	fixed: false, //不固定
	 	maxmin: true,
	 	content: url,
	 	btn: [_title,'取消'],
	 	yes:function(index,layero){
	 		var iframe = window[layero.find('iframe')[0]['name']];
		  	iframe.$("#mark").val("0"); // 标示可以提交表单
			if (iframe.dd_form_check()) { // 按钮返回验证表单函数
				var _data = iframe.$("#myform").serialize();
				$.ajax({
					type: "POST",
					dataType: "json",
					url: url,
					data: _data,
					success: function(data) {
						if (data.status == 1) {
							dd_tips(data.code, 2, 1);
							layer.close(index);
							setTimeout("window.location.reload(true)", 1000)
						} else {
							layer.msg(data.code, {icon: 5});
							return false
						}
					},
					error: function(HttpRequest, ajaxOptions, thrownError) {
						layer.msg('ajax错误');
					}
				});
			}else{
				return false;
			}
	  	}

	})
}

function dd_dialog(url, func) {

	switch (func) {
		case "add":
			var _title = lang["add"];
			break;
		case "edit":
			var _title = lang["edit"];
			break;
		default:
			return false;
			break
	}

	layer.open({
	  type: 2,
	  title: _title,
	  area: ['60%', '70%'],
	  fixed: false, //不固定
	  maxmin: true,
	  content: url,
	  btn: [_title],
	  yes: function(index,layero){
		  	var iframe = window[layero.find('iframe')[0]['name']];
		  	iframe.$("#mark").val("0"); // 标示可以提交表单
			if (iframe.dd_form_check()) { // 按钮返回验证表单函数
				var _data = iframe.$("#myform").serialize();
				$.ajax({
					type: "POST",
					dataType: "json",
					url: url,
					data: _data,
					success: function(data) {
						if (data.status == 1) {
							dd_tips(data.code, 2, 1);
							//iframe.close();
							setTimeout("window.location.reload(true)", 1000)
						} else {
							layer.msg(data.code, {icon: 5});
							return false
						}
					},
					error: function(HttpRequest, ajaxOptions, thrownError) {
						layer.msg('ajax错误');
					}
				});

			}else{

			}
			return false;
		}
	});
}

function dd_dialog_url(url, func) {

	switch (func) {
		case "add":
			var _title = lang["add"];
			break;
		case "edit":
			var _title = lang["edit"];
			break;
		default:
			return false;
			break
	}

	layer.open({
	  type: 2,
	  title: _title,
	  area: ['60%', '70%'],
	  fixed: false, //不固定
	  maxmin: true,
	  content: url,
	  btn: [_title],
	  yes: function(index,layero){
		  	var iframe = window[layero.find('iframe')[0]['name']];
		  	iframe.$("#mark").val("0"); // 标示可以提交表单
			if (iframe.dd_form_check()) { // 按钮返回验证表单函数
				var _data = iframe.$("#myform").serialize();
				$.ajax({
					type: "POST",
					dataType: "json",
					url: url,
					data: _data,
					success: function(data) {
						if (data.status == 1) {
							dd_tips(data.code, 2, 1);
							layer.close(index);
							setTimeout("window.location.reload(true)", 1000)
						} else {
							layer.msg(data.code, {icon: 5});
							return false
						}
					},
					error: function(HttpRequest, ajaxOptions, thrownError) {
						layer.msg('ajax错误');
					}
				});

			}else{

			}
			return false;
		}
	});
}

function dd_upload_files(name, url, pan, count) {

    var size = $("#" + name + "-sort-items li").length;
    var total = count - size;
    layer.open({
      type: 2,
      title: lang["upload"],
      shadeClose: true,
      btn:['确定','取消'],
      shade: 0.8,
      area: ['80%', '80%'],
      content: url + "&count=" + total,
      yes: function(index,layero){
        
        var iframe = window[layero.find('iframe')[0]['name']];

        if (!iframe.document.body) {
            dd_tips("iframe loading");
            return false
        }

        var value = iframe.document.getElementById("att-status").innerHTML;
        if (value == "" || value == undefined) {
            dd_tips(lang["notselectfile"]);
            return false;

        } else {
            var file = value.split("|");
            for (var i in file) {
                var filepath = file[i];
                var id = parseInt(size) + parseInt(i);
                if (filepath) {
                    var info = filepath.split(",");
                    if ($("#" + name + '-sort-items [value="' + info[0] + '"]').length > 0) {
                        dd_tips(fc_lang[27]);
                        return false
                    }
                    if (!info[0] || info[0] == undefined) {
                        info[0] = ''
                    }
                    if (!info[3] || info[3] == undefined) {
                        info[3] = info[0]
                    }
                    info[3] = dd_remove_ext(info[3]);
                    var c = "";
                    c += '<li id="files_' + name + "_" + id + '" list="' + id + '" style="cursor:move;">';
                    c += '<input type="hidden" value="' + info[0] + '" name="data[' + name + '][file][]" id="fileid_' + name + "_" + id + '" />';
                    c += '<label><input type="text" class="layui-input" style="width:300px;height:30px;" value="' + info[3] + '" name="data[' + name + '][title][]" /></label>\t';
                    c += '<label><a href="javascript:;" class="layui-btn layui-btn-xs layui-btn-warm" onclick="dd_remove_file(\'' + name + "','" + id + "')\">";
                    c += '<i class="fa fa-trash"></i>删除</a></label>\t';
                    c += '<label id="span_' + name + '_' + id + '"><a href="javascript:;" class="layui-btn layui-btn-xs layui-btn-primary" onclick="dd_show_file_info(\'' + info[0] + "')\">";
                    c += '<i class="fa fa-search"></i>查看</a></label>\t';
                    c += "</li>";
                    $("#" + name + "-sort-items").append(c);
                    layer.close(index);
                }
            }

        }

      }

    });
}

function dd_upload_files2(url) {
	top.dialog({
		title: lang["upload"],
		quickClose: true,
		url: url,
		width: 550,
		height: 400,
		okValue: lang['ok'],
		ok: function() {
			window.location.reload(true)
		}
	}).show();
};

function dd_copy_module(url) {
	
	$.ajax({type: "GET", url:url, dataType:'text', success: function (text) {
			layer.open({
			  type: 2,
			  title: '添加模型',
			  shadeClose: true,
			  shade: 0.8,
			  area: ['430px', '60%'],
			  content: url,
			  btn: ['添加'],
			  yes: function(index,layero){
			  	//得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
			  	var iframeWin = window[layero.find('iframe')[0]['name']];

			  	iframeWin.$("#mark").val("0"); // 标示可以提交表单
				if (iframeWin.dd_form_check()) { // 按钮返回验证表单函数
					var _data = iframeWin.$("#myform").serialize();
					$.ajax({type: "POST",dataType:"json", url: url, data: _data,
						success: function(data) {
							if (data.status == 1) {
								dd_tips(data.code, 3, 1);
								iframeWin.close();
								setTimeout("parent.location.reload();", 1000);
							} else {
								dd_tips(data.code, 5); 
								return true;
							}
						},
						error: function(HttpRequest, ajaxOptions, thrownError) {
							alert(HttpRequest.responseText);
						}
					});
				}
				return false;
			  }
			});
		}
	});
}

function dd_confirm_move() {
	layer.confirm('{fc_lang("您确定要这样操作吗？")}', {
		title:lang["tips"],
      	btn: ['确定','取消'] //按钮
    }, function(){

    	$('#action').val('move');
		var _data = $("#myform").serialize();
		var _url = window.location.href;
		if ((_data.split('ids')).length-1 <= 0) {
			dd_tips(lang['select_null'], 2);
			return true;
		}
		// 将表单数据ajax提交验证
		$.ajax({type: "POST",dataType:"json", url: _url, data: _data,
			success: function(data) {
				//验证成功
				if (data.status == 1) {
					dd_tips(data.code, 3, 1);
					$("input[name='ids[]']:checkbox:checked").each(function(){
						$.post("{$html_url}c=show&m=create_html&id="+$(this).val(), {}, function(){});
					});
					$.post("{$html_url}c=home&m=create_list_html&id="+$('#move_id').val(), {}, function(){});
					setTimeout('window.location.reload(true)', 3000); // 刷新页
					return true;
				} else {
					dd_tips(data.code, 3, 2);
					return true;
				}
			},
			error: function(HttpRequest, ajaxOptions, thrownError) {
				alert(HttpRequest.responseText);
			}
		});

    }, function(){
    	//取消回调

    });
}

function dd_input_files(name, count) {
    console.log("#" + name + "-sort-items li");
    var size = $("#" + name + "-sort-items li").length;
    var total = count - size;
    if (total <= 0) {
        dd_tips(fc_lang[42]);
        return
    }
    var id = size + 1;
    var url = '/index.php?c=api&&m=upload_input';
    $.ajax({
        type: "GET",
        url: url,
        dataType: "text",
        success: function(text) {

        	layer.open({
			  type: 1,
			  title: lang["input"],
			  shadeClose: true,
			  shade: 0.8,
			  area: ['70%', '60%'],
			  content: text,
			  btn: lang['ok'],
			  yes: function(index,layero){

			  	var title = $(layero).find("#dd_title").val();
                var furl = $(layero).find("#dd_url").val();
                if (!title || !furl) {
                    dd_tips(fc_lang[43]);
                    return false
                }
                var c = "";
                c += '<li id="files_' + name + "_" + id + '" list="' + id + '" style="cursor:move;">';
                c += '<input type="hidden" value="' + furl + '" name="data[' + name + '][file][]" id="fileid_' + name + "_" + id + '" />';
                c += '<label><input type="text" class="layui-input" style="width:300px;height:30px;" value="' + title + '" name="data[' + name + '][title][]" /></label>\t';
                c += '<label><a href="javascript:;" class="layui-btn layui-btn-xs layui-btn-warm" onclick="dd_remove_file(\'' + name + "','" + id + "')\">";
                c += '<i class="fa fa-trash"></i>删除</a></label>\t';
                c += '<label id="span_' + name + '_' + id + '"><a href="javascript:;" class="layui-btn layui-btn-xs" onclick="dd_show_file_info(\'' + furl + "')\">";
                c += '<i class="fa fa-search"></i>查看</a></label>\t';
                c += "</li>";
                $("#" + name + "-sort-items").append(c);

                layer.close(index);
			  }
			});

        },
        error: function(HttpRequest, ajaxOptions, thrownError) {
            dd_alert(HttpRequest.responseText)
        }
    })
}

//排序
$("#dd_confirm_order").click(function(){
	dd_confirm_order();
});

//删除
$("#dd_confirm_del_all").click(function(){
	dd_confirm_del_all();
});

//移动
$("#dd_confirm_move").click(function(){
	dd_confirm_move();
});

//移动
$("#dd_dialog_url").click(function(){
	dd_dialog_url($(this).data('url'),$(this).data('type'));
});

$(function(){
	layui.use("form", function(){
		var form = layui.form;
		//全选
		form.on('checkbox(dd_select_all)',function(data){
			if ($("#dd_select").prop("checked")) {
				$(".dd_select").prop("checked", true);
				form.render();
			} else {
				$(".dd_select").prop("checked", false);
				form.render();
			}
		});
	
		//表单
		form.on('checkbox(checkedf)',function(data){
			if($("#dd_check_"+data.value).prop("checked")){
				$("#dd_check_"+data.value).prop("checked", false);
			}else{
				$("#dd_check_"+data.value).prop("checked", true);
			}
		});
	
	});
});
