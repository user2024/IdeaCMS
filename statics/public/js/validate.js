function d_tips(name, status, code) {
	var obj = $("#dd_" + name + "_tips");
	var value = obj.html();
	if (!value) {
		obj.html("")
	}
	if (status) {
		if (code) {
			dd_tips(code, 3, 1)
		}
	} else {
		$("#dd_" + name).focus();
		if (code) {
			dd_tips(code)
		}
	}
	top.$(".page-loading").remove()
}

function check_title(t) {
	var val = $("#dd_title").val();
	var mod = $("#dd_module").val();
	var id = $("#dd_id").val();
	$.get("/index.php?c=api&m=checktitle&title=" + val + "&module=" + mod + "&id=" + id + "&rand=" + Math.random(), function(data) {
		if (data) {
			if (t == "1") {
				dd_tips(data)
			} else {
				$("#dd_title_tips").html(data)
			}
		} else {
			if (t == "1") {
				dd_tips('', 3, 1)
			} else {
				$("#dd_title_tips").html("")
			}
		}
	})
}
function get_keywords(to) {
	var title = $("#dd_title").val();
	if ($("#dd_" + to).val()) {
		return false
	}
	$.get("/index.php?c=api&m=getkeywords&title=" + title + "&rand=" + Math.random(), function(data) {
		layer.msg(data);
		$("#dd_" + to).val(data);
		//$("#dd_" + to).tagsinput('add', data)
	})
}
function d_topinyin(name, from, letter) {
	var val = $("#dd_" + from).val();
	if ($("#dd_" + name).val()) {
		return false
	}
	$.get("/index.php?c=api&m=pinyin&name=" + val + "&rand=" + Math.random(), function(data) {
		$("#dd_" + name).val(data);
		if (letter) {
			$("#dd_letter").val(data.substr(0, 1))
		}
	})
}
function d_required(name) {
	if ($("#dd_" + name).val() == "") {
		d_tips(name, false);
		return true
	} else {
		d_tips(name, true);
		return false
	}
}
function d_isemail(name) {
	var val = $("#dd_" + name).val();
	var reg = /^[-_A-Za-z0-9]+@([_A-Za-z0-9]+\.)+[A-Za-z0-9]{2,3}$/;
	if (reg.test(val)) {
		d_tips(name, true);
		return false
	} else {
		d_tips(name, false);
		return true
	}
}
function d_isurl(name) {
	var val = $("#dd_" + name).val();
	var reg = /http(s)?:\/\/([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
	var Exp = new RegExp(reg);
	if (Exp.test(val) == true) {
		d_tips(name, true);
		return false
	} else {
		d_tips(name, false);
		return true
	}
}
function d_isdomain(name) {
	var val = $("#dd_" + name).val();
	if (val.indexOf("/") > 0) {
		d_tips(name, false);
		return true
	} else {
		d_tips(name, true);
		return false
	}
};