<?php

/**
* ci email
*/
class Tmail 
{
	public $config;
	public $error;
	private $ci;

	public function __construct()
	{
		$this->ci = &get_instance();
	}

	//配置项
	public function set($config)
	{
		$this->config = array(
			'protocol' => 'smtp',
	        'smtp_host' => $config['smtp_host'],
			'smtp_port' => $config['smtp_port'],
			'smtp_user' => $config['smtp_user'],
			'smtp_pass'	=> $config['smtp_pass'],
			'wordwrap' => TRUE,
			'charset' =>'utf-8',
	    );
	}

	public function send($toemail, $subject, $message)
	{
		if (!$toemail) {
			$this->runlog($this->config['smtp_host'].' - '.$this->config['smtp_user'].' - '.$toemail,'收件人不能为空');
		}elseif (!$subject) {
			$this->runlog($this->config['smtp_host'].' - '.$this->config['smtp_user'].' - '.$toemail,'邮件标题未填写');
		}elseif (!$message) {
			$this->runlog($this->config['smtp_host'].' - '.$this->config['smtp_user'].' - '.$toemail,'邮件内容不能为空');
		}

		$this->ci->load->library('email');
		if ($this->config['smtp_port'] == 465) {
			$this->config['smtp_crypto'] = 'ssl';//使用ssl
		}
		$this->config['crlf'] = "\r\n";
		$this->config['newline'] = "\r\n";
		$this->config['wordwrap'] = TRUE;
		$this->config['mailtype'] = 'text';

		$this->ci->email->initialize($this->config);
		$this->ci->email->from($this->config['smtp_user'], 'IDEAWEB');
		//$this->email->cc($this->post('mail_user'));//抄送
		$this->ci->email->to($toemail);
		$this->ci->email->subject($subject);
		$this->ci->email->message($message);
		if($this->ci->email->send() == false){
		    $this->runlog($this->config['smtp_host'].' - '.$this->config['smtp_user'].' - '.$toemail,'发送失败');
		    return false;
		}
		return true;
	}

	public function error()
	{
		return $this->error;
	}


	public function runlog($server, $msg)
	{
		$this->error = $msg;
		@file_put_contents(WEBPATH.'cache/mail_error.log', date('Y-m-d H:i:s').' ['.$server.'] '.str_replace(array(chr(13), chr(10)), '', $msg).PHP_EOL, FILE_APPEND);
	}
	
}